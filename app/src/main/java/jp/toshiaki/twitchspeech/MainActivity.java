package jp.toshiaki.twitchspeech;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageButton;

import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.os.Build;
import android.os.IBinder;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.SeekBar;
import android.content.SharedPreferences;
import android.content.Context;
import android.content.ServiceConnection;

import java.lang.ref.WeakReference;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;

public class MainActivity extends AppCompatActivity implements TWSSpeechDelegate {
    private ExecutorService _executor = Executors.newFixedThreadPool(2);
    private float _speechRate = 1.2f;
    private long _speechIntervalMillisec = 2000;
    private float _speechPitch = 1.0f;
    private TWSService _service;
    private ServiceConnection _serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            TWSService.TWSServiceBinder binder = (TWSService.TWSServiceBinder)service;
            _service = binder.getService();
            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(MainActivity.this, R.layout.comment_row, _service._comments);
            ListView commentView = findViewById(R.id.commentView);
            commentView.setAdapter(arrayAdapter);
            commentView.setSelection(arrayAdapter.getCount()-1);
            _service._speechRate = _speechRate;
            _service._speechIntervalMillisec = _speechIntervalMillisec;
            _service._speechPitch = _speechPitch;
            binder.setDelegate(MainActivity.this);
            setStatusText(_service._statusText);
            if (_service.isConnected()) {
                AppCompatImageButton button = findViewById(R.id.button);
                button.setImageResource(R.drawable.ic_stop_black_24dp);
                EditText channel = findViewById(R.id.channelName);
                channel.setEnabled(false);
            }
        }
        @Override
        public void onServiceDisconnected(ComponentName name) {
            _service = null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //this.getSupportActionBar().hide();
        SharedPreferences prefs = getSharedPreferences("Prefs", Context.MODE_PRIVATE);
        String channel = prefs.getString("Channel","" );
        ((EditText)findViewById(R.id.channelName)).setText(channel);
        final SeekBar seekBarInterval = (SeekBar)findViewById(R.id.seekBarInterval);
        _speechIntervalMillisec =  prefs.getLong("SpeechInterval",2000 );
        seekBarInterval.setProgress((int)_speechIntervalMillisec);
        seekBarInterval.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                _speechIntervalMillisec = progress;
                if (_service != null) _service._speechIntervalMillisec = _speechIntervalMillisec;
            }
            public void onStartTrackingTouch(SeekBar seekBar) {
            }
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        final SeekBar seekBarSpeed = (SeekBar)findViewById(R.id.seekBarSpeed);
        _speechRate =  prefs.getFloat("SpeechRate",1.2f );
        seekBarSpeed.setProgress((int)(_speechRate*100-80));
        seekBarSpeed.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                _speechRate = (progress + 80) / 100.0f;
                if (_service != null) _service._speechRate = _speechRate;
            }
            public void onStartTrackingTouch(SeekBar seekBar) {
            }
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        final SeekBar seekBarPitch = (SeekBar)findViewById(R.id.seekBarPitch);
        _speechPitch =  prefs.getFloat("SpeechPitch",1.0f );
        seekBarPitch.setProgress((int)(_speechPitch*100-50));
        seekBarPitch.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                _speechPitch = (progress + 50) / 100.0f;
                if (_service != null) _service._speechPitch = _speechPitch;
            }
            public void onStartTrackingTouch(SeekBar seekBar) {
            }
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        SharedPreferences prefs = getSharedPreferences("Prefs", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("Channel", ((EditText)findViewById(R.id.channelName)).getText().toString());
        editor.putLong("SpeechInterval", _speechIntervalMillisec);
        editor.putFloat("SpeechRate", _speechRate);
        editor.putFloat("SpeechPitch", _speechPitch);
        editor.apply();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = new Intent(this, TWSService.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(intent);
            bindService(intent, _serviceConnection, 0);
        }
        else{
            startService(intent);
            bindService(intent, _serviceConnection, 0);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        unbindService(_serviceConnection);
        //_service = null;
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    /*@Override
    public void onBackPressed() {
        Intent mainActivity = new Intent(Intent.ACTION_MAIN);
        mainActivity.addCategory(Intent.CATEGORY_HOME);
        mainActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(mainActivity);
    }*/

    public void toggleConnection(View view) throws Exception {
        if (_service == null) return;
        EditText field = findViewById(R.id.channelName);
        String channelName = field.getText().toString();
        if (channelName.length() == 0) return;
        if (!_service.isConnected()) {
            ((AppCompatImageButton)view).setImageResource(R.drawable.ic_stop_black_24dp);
            field.setEnabled(false);
            _service.startConnection(channelName);
        } else {
            setStatusText("切断中...");
            view.setEnabled(false);
            _service.stopConnection();
            ((AppCompatImageButton)view).setImageResource(R.drawable.ic_play_arrow_black_24dp);
            field.setEnabled(true);
            view.setEnabled(true);
        }
    }

    public void skipIfPlaying(View view) {
        if (_service == null) return;
        _service.skipIfPlaying();
    }

    public void speechText(String textToSpeech) {
    }

    public void setStatusText(String text) {
        final String statusText = text;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView view = findViewById(R.id.status);
                view.setText(statusText);
            }
        });
    }

    public void putComment(final String text) {
    }

    public void newCommentArrived() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ListView commentView = findViewById(R.id.commentView);
                ArrayAdapter<String> adapter = (ArrayAdapter<String>) commentView.getAdapter();
                adapter.notifyDataSetChanged();
                commentView.smoothScrollToPosition(adapter.getCount()-1);
            }
        });
    }
}
