package jp.toshiaki.twitchspeech;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.io.IOException;
import java.nio.channels.SocketChannel;
import java.nio.channels.Selector;
import java.nio.channels.SelectionKey;
import java.nio.charset.StandardCharsets;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Iterator;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.regex.Pattern;


public class TWSIrcClient implements Runnable {
    private final String _defaultServer = "irc.chat.twitch.tv";
    private final int _defaultPort = 6667;
    private String _nickName;
    private String _password;
    private final StringBuilder _commandBuffer = new StringBuilder();
    private List<String> _channels = new ArrayList<String>();
    private SocketChannel _socketChannel;
    private Selector _selector;
    private boolean _running;
    private ExecutorService _executor;
    private Future _future;
    private TWSSpeechDelegate _delegate;
    private int _connectionStatus = -1;

    public TWSIrcClient(String nick, String pass, ExecutorService executor, TWSSpeechDelegate delegate)
    {
        _nickName = nick;
        _password = pass;
        _executor = executor;
        _delegate = delegate;
    }

    public TWSIrcClient(ExecutorService executor, TWSSpeechDelegate delegate)
    {
        Random random = new Random();
        _nickName = "justinfan" + (1000 + random.nextInt(80000));
        _password = "SCHMOOPIIE";
        _executor = executor;
        _delegate = delegate;
    }

    private boolean connect(String server, int port)
    {
        if (_socketChannel != null && _socketChannel.isConnected()) disconnect();
        try {
            InetSocketAddress address = new InetSocketAddress(InetAddress.getByName(server), port);
            _socketChannel = SocketChannel.open();
            _socketChannel.connect(address);
            _selector = Selector.open();

        } catch (Exception ex) {
            System.err.println(ex.toString());
            return false;
        }
        return true;
    }

    private boolean connect()
    {
        return this.connect(_defaultServer, _defaultPort);
    }

    private void disconnect()
    {
        _connectionStatus = -1;
        try {
            _socketChannel.close();
            _socketChannel = null;
            _selector.close();
            _selector = null;
        } catch (Exception ex) {
            System.err.println(ex.toString());
        }
    }

    private void login()
    {
        try {
            String cmd = String.format("PASS %s\r\nNICK %s\r\n", _password, _nickName);
            _socketChannel.write(ByteBuffer.wrap(cmd.getBytes(StandardCharsets.UTF_8)));
        } catch (Exception ex) {
            System.err.println(ex.toString());
        }
    }

    public void join(String channel) {
        if (_channels.contains(channel)) return;
        synchronized (_commandBuffer) {
            _commandBuffer.append(String.format("JOIN %s\r\n", channel));
        }
    }

    public void part(String channel) {
        synchronized (_commandBuffer) {
            _commandBuffer.append(String.format("PART %s\r\n", channel));
        }
    }

    public void part() {
        if (_channels.size() == 0) return;
        synchronized (_commandBuffer) {
            _commandBuffer.append(String.format("PART %s\r\n", _channels.get(0)));
        }
    }

    public void execCommand(String command) {
        synchronized (_commandBuffer) {
            _commandBuffer.append(String.format("%s\r\n", command));
        }
    }

    public void startRunning() {
        if (_running) return;
        _running = true;
        //_executor = Executors.newSingleThreadExecutor();
        _future = _executor.submit(this);
    }

    public void stopRunning() throws Exception {
        if (!_running) return;
        _running = false;
        if (_selector != null) _selector.wakeup();
        _future.get();
        //_executor.shutdown();
        //_executor = null;
        _future = null;
        _channels.clear();
    }

    public boolean isRunning() {
        return _running;
    }

    public void run()
    {
        ByteBuffer recvBuffer = ByteBuffer.allocate(4096);
        int sleepSecond = 1;
        while (_running)
        {
            _delegate.setStatusText("接続中...");
            if ((_socketChannel == null || !_socketChannel.isConnected()) && !connect())
            {
                try {
                    Thread.sleep(sleepSecond * 1000);
                } catch (Exception ex) {
                }
                if (sleepSecond < 16) sleepSecond <<= 1;
                continue;
            }
            sleepSecond = 1;
            //login();
            _delegate.setStatusText("接続済み");
            try {
                _socketChannel.configureBlocking(false);
                SelectionKey selectionKey = _socketChannel.register(_selector, SelectionKey.OP_READ);
                _connectionStatus = 0;
                _executor.submit(new Runnable() {
                    @Override
                    public void run() {
                        long started = System.currentTimeMillis();
                        try {
                            while (_running && _connectionStatus < 2) {
                                Thread.sleep(100);
                                if (System.currentTimeMillis() - started > 3000) {
                                    System.err.println("login lock detected!");
                                    _connectionStatus = 0;
                                    _selector.wakeup();
                                    break;
                                }
                            }
                        } catch (Exception ex) {
                        }
                    }
                });
                IrcMainLoop:
                while (_running && _socketChannel.isConnected())
                {
                    if (_connectionStatus == 0) {
                        selectionKey.interestOps(SelectionKey.OP_READ|SelectionKey.OP_WRITE);
                    }
                    else if (_connectionStatus == 2 && _commandBuffer.length() > 0) {
                        selectionKey.interestOps(SelectionKey.OP_READ|SelectionKey.OP_WRITE);
                    }
                    if (_selector.select(500) > 0)
                    {
                        Set<SelectionKey> keys = _selector.selectedKeys();
                        Iterator<SelectionKey> keyIterator = keys.iterator();
                        while (keyIterator.hasNext()) {
                            SelectionKey key = keyIterator.next();
                            keyIterator.remove();
                            if (key.isReadable()) {
                                try {
                                    SocketChannel socket = (SocketChannel)key.channel();
                                    socket.read(recvBuffer);
                                } catch (Exception ex) {
                                    System.err.println("Socket read failed: " + ex.toString());
                                    break IrcMainLoop;
                                }
                                int ret = recvBuffer.position();
                                int pos = 0;
                                recvBuffer.flip();
                                byte[] buffer = recvBuffer.array();
                                while (pos < ret)
                                {
                                    try
                                    {
                                        int tmp = pos;
                                        while (tmp < ret - 1)
                                        {
                                            if (buffer[tmp] == '\r' && buffer[tmp + 1] == '\n') break;
                                            tmp++;
                                        }
                                        if (tmp == ret - 1) break;
                                        int end = tmp;
                                        int start;
                                        String name = "";
                                        String command = "";
                                        String param = "";
                                        String data = "";
                                        if (buffer[pos] == ':')
                                        {
                                            start = pos + 1;
                                            while (pos < end)
                                            {
                                                if (buffer[pos++] == ' ') break;
                                            }
                                            if (pos == end) break;
                                            name = new String(buffer, start, pos - 1 - start, StandardCharsets.UTF_8);
                                        }
                                        start = pos;
                                        while (pos < end)
                                        {
                                            if (buffer[pos++] == ' ') break;
                                        }
                                        if (pos == end) break;
                                        command = new String(buffer, start, pos - 1 - start, StandardCharsets.UTF_8);
                                        start = pos;
                                        while (pos < end)
                                        {
                                            if (buffer[pos++] == ':') break;
                                        }
                                        if (pos < end)
                                        {
                                            data = new String(buffer, pos, end - pos, StandardCharsets.UTF_8);
                                            pos -= 2;
                                        }
                                        if (pos > start) param = new String(buffer, start, pos - start, StandardCharsets.UTF_8);
                                        //Console.WriteLine("{0},{1},{2},{3}|",name,command,param,data);
                                        boolean echoBack = true;
                                        if (command.equals("PING"))
                                        {
                                            synchronized (_commandBuffer)
                                            {
                                                _commandBuffer.append(String.format("PONG :%s\r\n", data));
                                            }
                                        }
                                        else if (command.equals("PRIVMSG"))
                                        {
                                            echoBack = false;
                                            if (BuildConfig.DEBUG) {
                                                String shortName = name.split("!")[0];
                                                System.out.printf("%s: %s\n", shortName, data);
                                            }
                                            _delegate.speechText(data);
                                        }
                                        else if (command.equals("JOIN"))
                                        {
                                            _delegate.setStatusText(param + " を読み上げ中");
                                            _channels.add(0, param);
                                        }
                                        else if (command.equals("PART"))
                                        {
                                            _channels.remove(param);
                                        }
                                        else if (_connectionStatus == 1 && (command.equals("376") || command.equals("422")))
                                        {
                                            _delegate.setStatusText("ログイン済み");
                                            _connectionStatus = 2;
                                            if (_channels.size() > 0) {
                                                synchronized (_commandBuffer)
                                                {
                                                    for (String channel : _channels)
                                                    {
                                                        _commandBuffer.append(String.format("JOIN %s\r\n", channel));
                                                    }
                                                }
                                                _channels.clear();
                                            }
                                        }
                                        if (BuildConfig.DEBUG && echoBack)
                                        {
                                            System.out.printf("%s - %s %s\n", command, param, data);
                                        }
                                        pos = end + 2;
                                    }
                                    catch (Exception ex)
                                    {
                                        pos = ret;
                                    }
                                }
                                //Console.WriteLine("{0} bytes left",ret-pos);
                                if (pos > 0 && pos < ret)
                                {
                                    recvBuffer.position(pos);
                                    recvBuffer.compact();
                                }
                                else recvBuffer.clear();
                                key.interestOps(SelectionKey.OP_READ);
                            }
                            if (key.isWritable())
                            {
                                if (_connectionStatus == 0) {
                                    _delegate.setStatusText("ログイン中...");
                                    try {
                                        String cmd = String.format("PASS %s\r\nNICK %s\r\n", _password, _nickName);
                                        SocketChannel socket = (SocketChannel) key.channel();
                                        socket.write(ByteBuffer.wrap(cmd.getBytes(StandardCharsets.UTF_8)));
                                        _connectionStatus = 1;
                                    } catch (Exception ex) {
                                        System.err.println("Socket write failed: " + ex.toString());
                                        break IrcMainLoop;
                                    }
                                }
                                else {
                                    synchronized (_commandBuffer) {
                                        try {
                                            if (_commandBuffer.length() > 0) {
                                                String cmd = _commandBuffer.toString();
                                                //System.err.println(cmd);
                                                SocketChannel socket = (SocketChannel) key.channel();
                                                socket.write(ByteBuffer.wrap(cmd.getBytes(StandardCharsets.UTF_8)));
                                                _commandBuffer.setLength(0);
                                            }
                                        } catch (Exception ex) {
                                            System.err.println("Socket write failed: " + ex.toString());
                                            break IrcMainLoop;
                                        }
                                    }
                                }
                                key.interestOps(SelectionKey.OP_READ);
                            }
                        }
                    }
                    else {
                        //System.err.println("timeout, repeating");
                    }
                }
            } catch (Exception ex) {
                System.err.println(ex.toString());
                //break;
            }
            boolean shouldNotClearCommandBuffer = false;
            if (_connectionStatus < 2 && _channels.size() == 0) shouldNotClearCommandBuffer = true;
            disconnect();
            recvBuffer.clear();
            if (!shouldNotClearCommandBuffer) {
                synchronized (_commandBuffer) {
                    _commandBuffer.setLength(0);
                }
            }
            _delegate.setStatusText("待機中");
        }
        //System.err.println("goodbye");
    }
}
