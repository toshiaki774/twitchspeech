package jp.toshiaki.twitchspeech;

public interface TWSSpeechDelegate {
    public void speechText(String textToSpeech);
    public void setStatusText(String text);
}
