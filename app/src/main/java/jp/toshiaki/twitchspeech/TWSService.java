package jp.toshiaki.twitchspeech;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Pattern;

public class TWSService extends Service implements TextToSpeech.OnInitListener, TWSSpeechDelegate {
    private ExecutorService _executor = Executors.newFixedThreadPool(2);
    private TWSIrcClient _client;
    private TextToSpeech _tts;
    public float _speechRate = 1.2f;
    public long _speechIntervalMillisec = 2000;
    public float _speechPitch = 1.0f;
    private SpeechTaskManager _speechManager = new SpeechTaskManager();
    private static final Pattern _urlPattern = Pattern.compile("https?://[-_.!~*'()a-zA-Z0-9;/?:@&=+$,%#]+");
    private static final Pattern _waraPattern1 = Pattern.compile("^[wｗ]$", Pattern.CASE_INSENSITIVE);
    private static final Pattern _waraPattern2 = Pattern.compile("([^a-zｗ])[wｗ]$", Pattern.CASE_INSENSITIVE);
    private static final Pattern _waraPattern3 = Pattern.compile("([^a-zｗ\\s])[wｗ](！|。|\\s)", Pattern.CASE_INSENSITIVE);
    private static final Pattern _warawaraPattern = Pattern.compile("[wｗ]{2,}", Pattern.CASE_INSENSITIVE);
    public final LinkedList<String> _comments = new LinkedList<String>();
    public String _statusText = "待機中";
    private final IBinder _binder = new TWSServiceBinder();
    private WeakReference<MainActivity> _delegate;
    private Timer _idleTimer;

    @Override
    public void onCreate() {
        super.onCreate();
        _tts = new TextToSpeech(this, this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (_client != null) {
            try {
                _client.stopRunning();
            } catch (Exception ex) {}
            _speechManager.stop();
        }
        _speechManager.shutdown();
        _executor.shutdown();
        if (_tts != null) _tts.shutdown();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Context context = getApplicationContext();
        String channelId = "default";
        intent = new Intent(context, MainActivity.class);
        int intentFlags = PendingIntent.FLAG_UPDATE_CURRENT;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            intentFlags |= PendingIntent.FLAG_IMMUTABLE;
        }
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, intentFlags);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId, context.getString(R.string.app_name), NotificationManager.IMPORTANCE_NONE);
            NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
            if (notificationManager != null) notificationManager.createNotificationChannel(channel);
        }
        Notification notification = new NotificationCompat.Builder(context, channelId)
                .setContentTitle("TwitchSpeech サービス")
                .setContentText("Twitch のコメント読み上げサービスが動作中です。")
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setContentIntent(pendingIntent)
                .build();
        startForeground(1, notification);
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return _binder;
    }

    @Override
    public void onRebind(Intent intent) {
        if (_idleTimer != null) {
            _idleTimer.cancel();
            _idleTimer = null;
        }
    }

    @Override
    public boolean onUnbind(Intent intent) {
        _delegate = null;
        if (!isConnected()) {
            _idleTimer = new Timer();
            _idleTimer.schedule(new TimerTask() {
                public void run() {
                    stopSelf();
                }
            }, 120000);
        }
        return true;
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS){
            Locale locale = Locale.JAPANESE;
            if (_tts.isLanguageAvailable(locale) < 0) {
                putComment("エラー: 音声合成エンジンで日本語が利用できません。");
                return;
            }
            _tts.setLanguage(locale);
            _tts.setOnUtteranceProgressListener(_speechManager);
            //_speechManager.start();
            //_speechManager.commitText("こんにちはhttp://www.google.com/起動終了https://www.google.com/");
            //_tts.speak("こんにちはhttp://www.google.com/起動終了https://www.google.com/", TextToSpeech.QUEUE_FLUSH, null, "test");
        } else {
            //System.err.println("TTS service initialization failure");
            putComment("エラー: 音声合成エンジンが利用できません。");
        }
    }

    public void startConnection(String channelName) {
        if (channelName.length() == 0) return;
        if (_client != null) return;
        _speechManager.start();
        _client = new TWSIrcClient(_executor, this);
        _client.startRunning();
        _client.join("#" + channelName);
    }

    public void stopConnection() throws Exception {
        if (_client == null) return;
        _client.stopRunning();
        _client = null;
        _speechManager.stop();
    }

    public boolean isConnected() {
        return _client != null;
    }

    public void speechText(String textToSpeech) {
        _speechManager.commitText(textToSpeech);
    }

    public void setStatusText(String text) {
        _statusText = text;
        if (_delegate != null) {
            MainActivity delegate = _delegate.get();
            if (delegate != null) delegate.setStatusText(text);
        }
    }

    public void skipIfPlaying() {
        _tts.stop();
    }

    public void putComment(final String text) {
        synchronized (_comments) {
            _comments.add(text);
            if (_comments.size() > 150) {
                while (_comments.size() > 100)
                    _comments.remove();
            }
        }
        if (_delegate != null) {
            MainActivity delegate = _delegate.get();
            if (delegate != null) delegate.newCommentArrived();
        }
    }

    public class TWSServiceBinder extends Binder {
        TWSService getService() {
            return TWSService.this;
        }
        void setDelegate(MainActivity delegate) {
            _delegate = new WeakReference<>(delegate);
        }
    }

    private class SpeechTaskManager extends UtteranceProgressListener {
        private ExecutorService _executor = Executors.newSingleThreadExecutor();
        private final Map<String, SpeechTaskManager.SpeechTask> _queuedTasks = new HashMap<String, SpeechTaskManager.SpeechTask>();

        private boolean _enabled;
        private long _serial = 0;

        public void commitText(String textToSpeech) {
            String id = Long.toString(_serial++);
            SpeechTaskManager.SpeechTask task = new SpeechTaskManager.SpeechTask(textToSpeech, id);
            synchronized (_queuedTasks) {
                _queuedTasks.put(id, task);
            }
            _executor.submit(task);
        }

        public void start() {
            synchronized (_queuedTasks) {
                _queuedTasks.clear();
            }
            _enabled = true;
        }

        public void stop() {
            _enabled = false;
            synchronized (_queuedTasks) {
                for (String key : _queuedTasks.keySet()) {
                    SpeechTaskManager.SpeechTask task = _queuedTasks.get(key);
                    task._speaking = false;
                }
            }
        }

        public void shutdown() {
            if (_enabled) stop();
            _executor.shutdown();
        }

        @Override
        public void onStart(String utteranceId) {
        } // Do nothing

        @Override
        public void onError(String utteranceId) {
            //System.err.println("speech task finished with error: " + utteranceId);
            synchronized (_queuedTasks) {
                SpeechTaskManager.SpeechTask task = _queuedTasks.get(utteranceId);
                if (task != null) {
                    _queuedTasks.remove(utteranceId);
                    task._speaking = false;
                }

            }
        }

        @Override
        public void onDone(String utteranceId) {
            //System.err.println("speech task finished: " + utteranceId);
            synchronized (_queuedTasks) {
                SpeechTaskManager.SpeechTask task = _queuedTasks.get(utteranceId);
                if (task != null) {
                    _queuedTasks.remove(utteranceId);
                    task._speaking = false;
                }
            }
        }

        @Override
        public void onStop(String utteranceId, boolean interrupted) {
            //System.err.println("speech task finished with stop: " + utteranceId);
            synchronized (_queuedTasks) {
                SpeechTaskManager.SpeechTask task = _queuedTasks.get(utteranceId);
                if (task != null) {
                    _queuedTasks.remove(utteranceId);
                    task._speaking = false;
                }

            }
        }

        private class SpeechTask implements Runnable {
            private String _textToSpeech;
            private String _id;
            private boolean _speaking;

            public SpeechTask(String textToSpeech, String id) {
                _textToSpeech = textToSpeech;
                _id = id;
            }
            public void run() {
                if (!_enabled) return;
                _speaking = true;
                putComment(_textToSpeech);
                _textToSpeech = _urlPattern.matcher(_textToSpeech).replaceAll("URL省略");
                _textToSpeech = _waraPattern1.matcher(_textToSpeech).replaceAll("わら");
                _textToSpeech = _waraPattern2.matcher(_textToSpeech).replaceAll("$1、わら");
                _textToSpeech = _waraPattern3.matcher(_textToSpeech).replaceAll("$1、わら$2");
                _textToSpeech = _warawaraPattern.matcher(_textToSpeech).replaceAll(" わらわら");
                _tts.setSpeechRate(_speechRate);
                _tts.setPitch(_speechPitch);
                _tts.speak(_textToSpeech, TextToSpeech.QUEUE_ADD, null, _id);
                try {
                    while (_speaking && _enabled) {
                        Thread.sleep(100);
                    }
                    if (_enabled) Thread.sleep(_speechIntervalMillisec);
                } catch (InterruptedException ex) {

                }
            }
        }
    }
}
